package DAL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class ModuloConexao {
		
	public static Connection conectaBD() {
		Connection conn = null;
		
		try {
			String url = "jdbc:mysql://localhost:3306/gestaoquadras?user=root&password=Sobradonovo18";
			conn = DriverManager.getConnection(url);
		} catch(SQLException erro) {
			JOptionPane.showMessageDialog(null,"ConexaoDAO" + erro.getMessage());
		}
		return conn;
	}
}
