package Telas;

import java.sql.*;
import DAL.ModuloConexao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaLogin extends JFrame {
	
	Connection conexao = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	public void logar() {
		String sql = "Select * from tbfuncionarios where login = ? and senha = ?";
		try {
			//as linhas abaixo preparam a consulta ao BD em funcao do que foi digitado
			// nas caixas de texto. O "?" � substituido pelo conteudo da variavel
			
			pst = conexao.prepareStatement(sql);
			pst.setString(1, (txtUsuario).getText());
			pst.setString(2, (txtSenha).getText());
			
			// a linha abaico executa a query
			rs = pst.executeQuery();
			
			//se existir usuario e senha correspondente
			if(rs.next()) {
				TelaMenuPrincipal principal = new TelaMenuPrincipal();
				principal.setVisible(true);
				this.dispose();
				conexao.close();
			} else {
				JOptionPane.showMessageDialog(null, "usuario ou senha invalido");
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			txtUsuario.setText(null);
			txtSenha.setText(null);
			}
	}


	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaLogin frame = new TelaLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaLogin() {
		
		conexao = ModuloConexao.conectaBD();
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 431, 237);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(38, 46, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Senha");
		lblNewLabel_1.setBounds(38, 91, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(82, 43, 299, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(82, 88, 299, 20);
		contentPane.add(txtSenha);
		
		JButton btnLogin = new JButton("Login");
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//chama o metodo logar
				logar();
			}
		});
		btnLogin.setBounds(292, 136, 89, 23);
		contentPane.add(btnLogin);
	}
}
