package Telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JRadioButtonMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaMenuPrincipal extends JFrame {

	private JPanel contentPane;

	Connection conexao = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaMenuPrincipal frame = new TelaMenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaMenuPrincipal() {
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 732, 516);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JList list = new JList();
		menuBar.add(list);
		
		JMenu MenuCadastro = new JMenu("Cadastro");
		menuBar.add(MenuCadastro);
		
		JMenuItem MenuItemClientes = new JMenuItem("Clientes");
		MenuItemClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TelaCadastroCliente cadastrocliente = new TelaCadastroCliente();
				cadastrocliente.setVisible(true);
			}
		});
		MenuCadastro.add(MenuItemClientes);
		
		JMenuItem MenuItemQuadras = new JMenuItem("Quadras");
		MenuItemQuadras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TelaCadastroQuadras cadastroquadras = new TelaCadastroQuadras();
				cadastroquadras.setVisible(true);
			}
		});
		MenuCadastro.add(MenuItemQuadras);
		
		JMenu MenuRelatorio = new JMenu("Relat\u00F3rio");
		menuBar.add(MenuRelatorio);
		
		JMenuItem MenuItemMidias = new JMenuItem("M\u00E9dias");
		MenuRelatorio.add(MenuItemMidias);
		
		JMenuItem MenuItemHorarios = new JMenuItem("Hor\u00E1rios");
		MenuRelatorio.add(MenuItemHorarios);
		
		JMenuItem MenuItemReservas = new JMenuItem("Reservas");
		MenuItemReservas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TelaReservas reservas = new TelaReservas();
				reservas.setVisible(true);
			}
		});
		MenuRelatorio.add(MenuItemReservas);
		
		JMenuItem MenuItemManutencoes = new JMenuItem("Manuten\u00E7\u00F5es");
		MenuRelatorio.add(MenuItemManutencoes);
		
		JMenu MenuAdministracao = new JMenu("Administra\u00E7\u00E3o");
		menuBar.add(MenuAdministracao);
		
		JMenuItem MenuItemEditarClientes = new JMenuItem("Editar Clientes");
		MenuAdministracao.add(MenuItemEditarClientes);
		
		JMenuItem MenuItemEditarQuadras = new JMenuItem("Editar Quadras");
		MenuAdministracao.add(MenuItemEditarQuadras);
		
		JMenuItem MenuItemEditarUsuarios = new JMenuItem("Editar Usu\u00E1rios");
		MenuAdministracao.add(MenuItemEditarUsuarios);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}