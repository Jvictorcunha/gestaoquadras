package Telas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import DAL.ModuloConexao;
import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;

import com.mysql.cj.xdevapi.Result;
import com.toedter.calendar.JDateChooser;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TelaReservas extends JFrame {

	private JPanel contentPane;
	private JComboBox cbCobertura;
	private JComboBox cbArquibancadas;
	private JComboBox cbBancoJogador;
	private JComboBox cbTipo;
	private JFormattedTextField txtCPF;
	private JTable tbQuadras;
	private JTable tbReservas;
	private JComboBox cbQuadras;
	private JScrollPane scrollPane1;
	private JComboBox cbCheckIn;
	private JComboBox cbCheckOut;
	private JDateChooser dtReserva;
	
	DefaultTableModel modelo;
	
	Connection conexao = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	
	void teste() {
		String inicio = "08:00:00";
		String termino = "18:00:00";
		
		List<String>horariosDisponiveis = new ArrayList<>();
		
		DateTimeFormatter formatadorData = DateTimeFormatter.ofPattern("HH:mm");
		LocalTime horarioTermino = LocalTime.parse(termino);
		
		LocalTime horario = LocalTime.parse(inicio);
		while(horario.isBefore(horarioTermino)) {
			horariosDisponiveis.add(horario.toString());
			horario = horario.plusMinutes(30);
		}
	}
	
	void formatarData() {
		try {
			MaskFormatter data = new MaskFormatter("####-##-##");
			data.setValidCharacters("0123456789");
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	void formatarCPF() {
		try {
			MaskFormatter cpf = new MaskFormatter("###.###.###-##");
			cpf.setValidCharacters("0123456789");
			cpf.install(txtCPF);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	void preenchertbQuadras() {
		modelo = (DefaultTableModel) tbQuadras.getModel();
		modelo.setNumRows(0);
		String sql = "SELECT * FROM tbquadras WHERE Tipo LIKE ? AND Cobertura LIKE ? AND Arquibancada LIKE ? AND Bancojogador LIKE ? AND idquadra not in(SELECT idquadra FROM tbreserva WHERE dia_reserva = ? AND check_in <= ? AND check_out >= ?)";
		try {
			
			pst = conexao.prepareStatement(sql);
			pst.setString(1,(cbTipo).getSelectedItem().toString());
			pst.setString(2,(cbCobertura).getSelectedItem().toString());
			pst.setString(3,(cbArquibancadas).getSelectedItem().toString());
			pst.setString(4,(cbBancoJogador).getSelectedItem().toString());
			pst.setString(5,((JTextField) dtReserva.getDateEditor().getUiComponent()).getText());
			pst.setString(6,(cbCheckOut).getSelectedItem().toString());
			pst.setString(7,(cbCheckIn).getSelectedItem().toString());
			rs = pst.executeQuery();
			
			while (rs.next()) {
				modelo.addRow(new Object[] {
						
						rs.getString(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
				});
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}	
	}
	
	void carregarCBQuadras() {
		cbQuadras.removeAllItems();
		String sql = "select idquadra from tbquadras where idquadra not in(select idquadra from tbreserva where dia_reserva = ? and check_in <= ? and check_out >= ?) and tipo = ? and cobertura =  ? and arquibancada = ? and bancojogador = ?";
		try {
			
			pst = conexao.prepareStatement(sql);
			pst.setString(1,((JTextField) dtReserva.getDateEditor().getUiComponent()).getText());
			pst.setString(2,(cbCheckIn).getSelectedItem().toString());
			pst.setString(3,(cbCheckOut).getSelectedItem().toString());
			pst.setString(4,(cbTipo).getSelectedItem().toString());
			pst.setString(5,(cbCobertura).getSelectedItem().toString());
			pst.setString(6,(cbArquibancadas).getSelectedItem().toString());
			pst.setString(7,(cbBancoJogador).getSelectedItem().toString());
			rs = pst.executeQuery();
			
			while (rs.next()) {
				cbQuadras.addItem(rs.getString(1));
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	void carregarCBInOut() {
		cbCheckIn.removeAllItems();
		cbCheckOut.removeAllItems();
		String sql = "select horario from tbhorarios where horario not in(select horario from tbhorarios where horario >= (select check_in from tbreserva) and horario <=(select check_out from tbreserva) and (select dia_reserva from tbreserva where dia_reserva = ? and idquadra = ?));";
		try {
			pst = conexao.prepareStatement(sql);
			pst.setString(1,((JTextField) dtReserva.getDateEditor().getUiComponent()).getText());
			pst.setString(2, (cbQuadras).getSelectedItem().toString());
			rs = pst.executeQuery();
			
			while (rs.next()) {
				cbCheckIn.addItem(rs.getString(1));
				cbCheckOut.addItem(rs.getString(1));
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	void preencherHorarios() {
		String sql = "select horario from tbhorarios";
		try {
			
			pst = conexao.prepareStatement(sql);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				cbCheckIn.addItem(rs.getString(1));
				cbCheckOut.addItem(rs.getString(1));
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	void preenchertbReservas() {
		modelo = (DefaultTableModel) tbReservas.getModel();
		modelo.setNumRows(0);
		String sql = "select * from tbreserva";
		try {
			
			pst = conexao.prepareStatement(sql);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				modelo.addRow(new Object[] {
						rs.getString(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5)
						
				});
				
				
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}		
	}
	
	void adicionar() {
		String sql = "insert into tbreserva(cpf_cliente, idquadra, dia_reserva, check_in, check_out)value(?,?,?,?,?)";
		try {
			pst = conexao.prepareStatement(sql);
			pst.setString(1, (txtCPF).getText());
			pst.setString(2, (cbQuadras).getSelectedItem().toString());
			pst.setString(3,((JTextField) dtReserva.getDateEditor().getUiComponent()).getText());
			pst.setString(4,(cbCheckIn).getSelectedItem().toString());
			pst.setString(5,(cbCheckOut).getSelectedItem().toString());
			// A linha abaixo atualiza a tabela de funcionarios com os dados adiciondos
			int adicionado = pst.executeUpdate();
			if(adicionado > 0) {
				JOptionPane.showMessageDialog(null, "Reserva realizada com sucesso!");
				txtCPF.setText(null);
				cbQuadras.setSelectedItem(null);
				dtReserva.setDate(null);;
				cbCheckIn.setSelectedItem(null);
				cbCheckOut.setSelectedItem(null);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Cliente Não Encontrado ");	
		}	
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaReservas frame = new TelaReservas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaReservas() {
		
		conexao = ModuloConexao.conectaBD();
		//preencherHorarios();
		//teste();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 730, 481);
		contentPane = new JPanel();
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setBorder(new CompoundBorder());
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		cbCobertura = new JComboBox();
		cbCobertura.setBackground(Color.WHITE);
		cbCobertura.setBounds(493, 54, 211, 20);
		cbCobertura.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		contentPane.add(cbCobertura);
		
		cbArquibancadas = new JComboBox();
		cbArquibancadas.setBackground(Color.WHITE);
		cbArquibancadas.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		cbArquibancadas.setBounds(110, 85, 211, 22);
		contentPane.add(cbArquibancadas);
		
		cbBancoJogador = new JComboBox();
		cbBancoJogador.setBackground(Color.WHITE);
		cbBancoJogador.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		cbBancoJogador.setBounds(493, 85, 211, 22);
		contentPane.add(cbBancoJogador);
		
		txtCPF = new JFormattedTextField();
		txtCPF.setBounds(110, 21, 211, 20);
		contentPane.add(txtCPF);
		formatarCPF();
		txtCPF.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCpf.setBounds(10, 24, 38, 14);
		contentPane.add(lblCpf);
		
		JLabel lblCobertura = new JLabel("COBERTURA");
		lblCobertura.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCobertura.setBounds(361, 57, 75, 14);
		contentPane.add(lblCobertura);
		
		JLabel lblArquibancada = new JLabel("ARQUIBANCADA");
		lblArquibancada.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblArquibancada.setBounds(10, 89, 90, 14);
		contentPane.add(lblArquibancada);
		
		JLabel lblBancoDeJogadores = new JLabel("BANCO DE JOGADOR");
		lblBancoDeJogadores.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBancoDeJogadores.setBounds(361, 89, 122, 14);
		contentPane.add(lblBancoDeJogadores);
		
		JScrollPane scrollPane1;
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(325, 230, 379, 201);
		contentPane.add(scrollPane1);
		
		tbReservas = new JTable();
		scrollPane1.setViewportView(tbReservas);
		tbReservas.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"Cliente", "N\u00BA Quadra", "DATA", "IN", "OUT"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbReservas.getColumnModel().getColumn(0).setPreferredWidth(116);
		tbReservas.getColumnModel().getColumn(1).setPreferredWidth(131);
		tbReservas.getColumnModel().getColumn(2).setPreferredWidth(90);
		tbReservas.getColumnModel().getColumn(3).setResizable(false);
		tbReservas.getColumnModel().getColumn(3).setPreferredWidth(90);
		tbReservas.getColumnModel().getColumn(4).setResizable(false);
		tbReservas.getColumnModel().getColumn(4).setPreferredWidth(90);
		
		// as linhas abaixo carregam a tabela de cadastro
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				preenchertbQuadras(); //ok
				preenchertbReservas(); //ok
				carregarCBQuadras(); //ok
				carregarCBInOut(); //ok
			}
		});
		btnBuscar.setBounds(606, 179, 98, 40);
		contentPane.add(btnBuscar);
		
		JLabel lblTipo = new JLabel("TIPO");
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipo.setBounds(361, 21, 38, 14);
		contentPane.add(lblTipo);
		
		cbTipo = new JComboBox();
		cbTipo.setModel(new DefaultComboBoxModel(new String[] {"Saibro", "Beach"}));
		cbTipo.setBounds(493, 15, 211, 20);
		contentPane.add(cbTipo);
		
		JLabel lblCheckin = new JLabel("Check-In");
		lblCheckin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCheckin.setBounds(10, 181, 90, 14);
		contentPane.add(lblCheckin);
		
		JLabel lblCheckout = new JLabel("Check-Out");
		lblCheckout.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCheckout.setBounds(361, 181, 90, 14);
		contentPane.add(lblCheckout);
		
		JLabel lblQuadra = new JLabel("QUADRA\r\n");
		lblQuadra.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblQuadra.setBounds(10, 57, 48, 14);
		contentPane.add(lblQuadra);
		
		cbQuadras = new JComboBox();
		cbQuadras.setModel(new DefaultComboBoxModel(new String[] {"-"}));
		cbQuadras.setBounds(110, 53, 211, 22);
		contentPane.add(cbQuadras);

		cbCheckIn = new JComboBox();
		cbCheckIn.setModel(new DefaultComboBoxModel(new String[] {"08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00"}));
		cbCheckIn.setBounds(102, 179, 219, 22);
		contentPane.add(cbCheckIn);
		
		cbCheckOut = new JComboBox();
		cbCheckOut.setModel(new DefaultComboBoxModel(new String[] {"08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00"}));
		cbCheckOut.setBounds(436, 177, 160, 22);
		contentPane.add(cbCheckOut);
		
		JLabel lblData = new JLabel("Data");
		lblData.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblData.setBounds(10, 138, 90, 14);
		contentPane.add(lblData);
		formatarData();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 230, 311, 201);
		contentPane.add(scrollPane);
		
		tbQuadras = new JTable();
		scrollPane.setViewportView(tbQuadras);
		tbQuadras.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"N\u00FAmero", "Tipo", "Arquibancada", "Cobertura", "Banco de Jogador"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		dtReserva = new JDateChooser();
		dtReserva.setDateFormatString("d/MM/y");
		dtReserva.setBounds(102, 138, 219, 20);
		contentPane.add(dtReserva);
		
		JButton btnReserva = new JButton("Reservar\r\n");
		btnReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adicionar();
			}
		});
		btnReserva.setBounds(606, 128, 98, 40);
		contentPane.add(btnReserva);
		
		
		tbQuadras.getColumnModel().getColumn(0).setResizable(false);
		tbQuadras.getColumnModel().getColumn(0).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(1).setResizable(false);
		tbQuadras.getColumnModel().getColumn(1).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(2).setResizable(false);
		tbQuadras.getColumnModel().getColumn(2).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(3).setResizable(false);
		tbQuadras.getColumnModel().getColumn(3).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(4).setResizable(false);
		tbQuadras.getColumnModel().getColumn(4).setPreferredWidth(120);
	}
}
