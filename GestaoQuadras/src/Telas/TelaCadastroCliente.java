package Telas;

import java.sql.*;
import java.text.ParseException;

import DAL.ModuloConexao;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;


public class TelaCadastroCliente extends JFrame {

	private JPanel contentPane;

	
	Connection conexao = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	DefaultTableModel modelo;


	private JTable tbClientes;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JFormattedTextField txtDDD;
	private JFormattedTextField txtTelefone;
	private JTextField txtRua;
	private JTextField txtNumero;
	private JTextField txtBairro;
	private JTextField txtCidade;
	private JComboBox cbEstado;
	private JFormattedTextField txtCep;
	private JFormattedTextField txtRG;
	private JFormattedTextField txtCPF;

	
	
	void formatarCPF() {
		try {
			MaskFormatter cpf = new MaskFormatter("###.###.###-##");
			cpf.setValidCharacters("0123456789");
			cpf.install(txtCPF);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	void formatarRG() {
		try {
			MaskFormatter rg = new MaskFormatter("##.###.###-#");
			rg.install(txtRG);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	void formatarCep() {
		try {
			MaskFormatter cep = new MaskFormatter("#####-###");
			cep.setValidCharacters("0123456789");
			cep.install(txtCep);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	void formatarTelefone() {
		try {
			MaskFormatter telefone = new MaskFormatter("#####-####");
			telefone.setValidCharacters("0123456789");
			telefone.install(txtTelefone);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	void formatarDDD() {
		try {
			MaskFormatter ddd = new MaskFormatter("(##)");
			ddd.setValidCharacters("0123456789");
			ddd.install(txtDDD);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	void adicionar() {
		String sql = "insert into tbclientes(nome, email, DDD, telefone, rua, numero, bairro, cidade, estado, cep, rg, cpf)value(?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			pst = conexao.prepareStatement(sql);
			pst.setString(1, txtNome.getText());
			pst.setString(2, txtEmail.getText());
			pst.setString(3, txtDDD.getText());
			pst.setString(4, txtTelefone.getText());
			pst.setString(5, txtRua.getText());
			pst.setString(6, txtNumero.getText());
			pst.setString(7, txtBairro.getText());
			pst.setString(8, txtCidade.getText());
			pst.setString(9, cbEstado.getSelectedItem().toString());
			pst.setString(10, txtCep.getText());
			pst.setString(11, txtRG.getText());
			pst.setString(12, txtCPF.getText());
			
			// A linha abaixo atualiza a tabela de funcionarios com os dados adiciondos
			int adicionado = pst.executeUpdate();
			if(adicionado > 0) {
				JOptionPane.showMessageDialog(null, "Cliente cadastrado com Sucesso");
				txtNome.setText(null);
				txtEmail.setText(null);
				txtDDD.setText(null);
				txtTelefone.setText(null);
				txtRua.setText(null);
				txtNumero.setText(null);
				txtBairro.setText(null);
				txtCidade.setText(null);
				cbEstado.setSelectedItem(null);
				txtCep.setText(null);
				txtRG.setText(null);
				txtCPF.setText(null);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);	
		}	
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroCliente frame = new TelaCadastroCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroCliente() {
		
		conexao = ModuloConexao.conectaBD();
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 730, 400);
		contentPane = new JPanel();
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setBorder(new CompoundBorder());
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 149, 694, 201);
		contentPane.add(scrollPane);
		
		tbClientes = new JTable();
		scrollPane.setViewportView(tbClientes);
		tbClientes.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Nome", "E-mail", "DDD", "Telefone", "Rua", "N\u00FAmero", "Bairro", "Cidade", "Estado", "Cep", "RG", "CPF"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbClientes.getColumnModel().getColumn(0).setPreferredWidth(90);
		tbClientes.getColumnModel().getColumn(1).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(2).setPreferredWidth(40);
		tbClientes.getColumnModel().getColumn(3).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(4).setPreferredWidth(100);
		tbClientes.getColumnModel().getColumn(5).setPreferredWidth(60);
		tbClientes.getColumnModel().getColumn(6).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(7).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(8).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(9).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(10).setPreferredWidth(120);
		tbClientes.getColumnModel().getColumn(11).setPreferredWidth(120);
		
		// as linhas abaixo carregam a tabela de cadastro
		modelo = (DefaultTableModel) tbClientes.getModel();
		modelo.setNumRows(0);
		
		String sql = "select * from tbclientes";
		try {
			pst = conexao.prepareStatement(sql);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				modelo.addRow(new Object[] {
						
						rs.getString(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7),
						rs.getString(8),
						rs.getString(9),
						rs.getString(10),
						rs.getString(11),
						rs.getString(12)
						
				});
			}
			
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
		JButton btnCadastrarQuadra = new JButton("Cadastrar");
		btnCadastrarQuadra.setBounds(606, 97, 98, 40);
		btnCadastrarQuadra.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				adicionar();
				// as linhas abaixo atualizam a tabela de cadastro
				modelo = (DefaultTableModel) tbClientes.getModel();
				modelo.setNumRows(0);
				
				String sql = "select * from tbclientes";
				try {
					pst = conexao.prepareStatement(sql);
					rs = pst.executeQuery();
					
					while (rs.next()) {
						modelo.addRow(new Object[] {
								
								rs.getString(1),
								rs.getString(2),
								rs.getString(3),
								rs.getString(4),
								rs.getString(5),
								rs.getString(6),
								rs.getString(7),
								rs.getString(8),
								rs.getString(9),
								rs.getString(10),
								rs.getString(11),
								rs.getString(12)
						});
					}
							
				} catch (Exception a) {
					JOptionPane.showMessageDialog(null, a);
				}
			}
		});
		contentPane.add(btnCadastrarQuadra);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 11, 35, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setBounds(192, 11, 35, 14);
		contentPane.add(lblEmail);
		
		JLabel lblDdd = new JLabel("DDD");
		lblDdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDdd.setBounds(368, 11, 24, 14);
		contentPane.add(lblDdd);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTelefone.setBounds(451, 11, 49, 14);
		contentPane.add(lblTelefone);
		
		JLabel lblRua = new JLabel("Rua");
		lblRua.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRua.setBounds(10, 36, 24, 14);
		contentPane.add(lblRua);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNumero.setBounds(192, 39, 44, 14);
		contentPane.add(lblNumero);
		
		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBairro.setBounds(305, 36, 35, 14);
		contentPane.add(lblBairro);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEstado.setBounds(10, 62, 38, 14);
		contentPane.add(lblEstado);
		
		JLabel lblCep = new JLabel("Cep");
		lblCep.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCep.setBounds(192, 62, 24, 14);
		contentPane.add(lblCep);
		
		JLabel lblCidade = new JLabel("Cidade\r\n");
		lblCidade.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCidade.setBounds(484, 36, 49, 14);
		contentPane.add(lblCidade);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRg.setBounds(316, 62, 16, 14);
		contentPane.add(lblRg);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCpf.setBounds(443, 62, 24, 14);
		contentPane.add(lblCpf);
		
		txtNome = new JTextField();
		txtNome.setBounds(52, 8, 130, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(237, 8, 121, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtDDD = new JFormattedTextField();
		txtDDD.setHorizontalAlignment(SwingConstants.CENTER);
		txtDDD.setBounds(402, 8, 28, 20);
		contentPane.add(txtDDD);
		formatarDDD();
		
		
		txtTelefone = new JFormattedTextField();
		txtTelefone.setHorizontalAlignment(SwingConstants.CENTER);
		txtTelefone.setBounds(510, 8, 86, 20);
		contentPane.add(txtTelefone);
		formatarTelefone();
		
		txtRua = new JTextField();
		txtRua.setBounds(52, 33, 130, 20);
		contentPane.add(txtRua);
		txtRua.setColumns(10);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(246, 33, 49, 20);
		contentPane.add(txtNumero);
		txtNumero.setColumns(10);
		
		txtBairro = new JTextField();
		txtBairro.setBounds(346, 33, 128, 20);
		contentPane.add(txtBairro);
		txtBairro.setColumns(10);
		
		txtCidade = new JTextField();
		txtCidade.setBounds(528, 33, 122, 20);
		contentPane.add(txtCidade);
		txtCidade.setColumns(10);
		
		cbEstado = new JComboBox();
		cbEstado.setModel(new DefaultComboBoxModel(new String[] {"Acre (AC)", "Alagoas (AL)", "Amapá (AP)", "Amazonas (AM)", "Bahia (BA)", "Ceará (CE)", "Distrito Federal (DF)", "Espírito Santo (ES)", "Goiás (GO)", "Maranhão (MA)", "Mato Grosso (MT)", "Mato Grosso do Sul (MS)", "Minas Gerais (MG)", "Pará (PA)", "Paraíba (PB)", "Paraná (PR)", "Pernambuco (PE)", "Piauí (PI)", "Rio de Janeiro (RJ)", "Rio Grande do Norte (RN)", "Rio Grande do Sul (RS)", "Rondônia (RO)", "Roraima (RR)", "Santa Catarina (SC)", "São Paulo (SP)", "Sergipe (SE)", "Tocantins (TO)"}));
		cbEstado.setBounds(52, 58, 130, 20);
		contentPane.add(cbEstado);
		
		txtCep = new JFormattedTextField();
		txtCep.setHorizontalAlignment(SwingConstants.CENTER);
		txtCep.setBounds(220, 58, 86, 20);
		contentPane.add(txtCep);
		txtCep.setColumns(10);
		formatarCep();
		
		txtRG = new JFormattedTextField();
		txtRG.setHorizontalAlignment(SwingConstants.CENTER);
		txtRG.setBounds(342, 58, 86, 20);
		contentPane.add(txtRG);
		formatarRG();
		
		txtCPF = new JFormattedTextField();
		txtCPF.setHorizontalAlignment(SwingConstants.CENTER);
		txtCPF.setBounds(477, 58, 98, 20);
		contentPane.add(txtCPF);
		formatarCPF();
	}
}
