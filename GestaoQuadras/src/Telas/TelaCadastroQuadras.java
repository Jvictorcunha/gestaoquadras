package Telas;

import java.sql.*;
import java.util.Vector;

import DAL.ModuloConexao;


import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.border.CompoundBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class TelaCadastroQuadras extends JFrame {

	private JPanel contentPane;
	private JComboBox cbCobertura;
	private JComboBox cbArquibancadas;
	private JComboBox cbBancoJogador;
	private JComboBox cbTipo;
	private JTable tbQuadras;
	
	DefaultTableModel modelo;
	
	Connection conexao = null;
	PreparedStatement pst = null;
	ResultSet rs = null;		
	
	void adicionar() {
		String sql = "insert into tbquadras(tipo, cobertura, arquibancada, bancojogador)value(?,?,?,?)";
		try {
			pst = conexao.prepareStatement(sql);
			pst.setString(1, cbTipo.getSelectedItem().toString());
			pst.setString(2, cbCobertura.getSelectedItem().toString());
			pst.setString(3, cbArquibancadas.getSelectedItem().toString());
			pst.setString(4, cbBancoJogador.getSelectedItem().toString());
			// A linha abaixo atualiza a tabela de funcionarios com os dados adiciondos
			int adicionado = pst.executeUpdate();
			if(adicionado > 0) {
				JOptionPane.showMessageDialog(null, "Quadra cadastrada com Sucesso");
				cbTipo.setSelectedItem(null);
				cbCobertura.setSelectedItem(null);
				cbArquibancadas.setSelectedItem(null);
				cbBancoJogador.setSelectedItem(null);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);	
		}	
	}
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroQuadras frame = new TelaCadastroQuadras();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroQuadras() {
		
		
		conexao = ModuloConexao.conectaBD();
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 730, 400);
		contentPane = new JPanel();
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setBorder(new CompoundBorder());
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		cbCobertura = new JComboBox();
		cbCobertura.setBackground(Color.WHITE);
		cbCobertura.setBounds(493, 21, 211, 20);
		cbCobertura.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		contentPane.add(cbCobertura);
		
		cbArquibancadas = new JComboBox();
		cbArquibancadas.setBackground(Color.WHITE);
		cbArquibancadas.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		cbArquibancadas.setBounds(110, 64, 211, 22);
		contentPane.add(cbArquibancadas);
		
		cbBancoJogador = new JComboBox();
		cbBancoJogador.setBackground(Color.WHITE);
		cbBancoJogador.setModel(new DefaultComboBoxModel(new String[] {"Sim", "Nao"}));
		cbBancoJogador.setBounds(493, 64, 211, 22);
		contentPane.add(cbBancoJogador);
		
		cbTipo = new JComboBox();
		cbTipo.setModel(new DefaultComboBoxModel(new String[] {"Saibro", "Beach"}));
		cbTipo.setBounds(110, 21, 211, 20);
		contentPane.add(cbTipo);
		
		JLabel lblTipo = new JLabel("TIPO");
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipo.setBounds(10, 24, 38, 14);
		contentPane.add(lblTipo);
		
		JLabel lblCobertura = new JLabel("COBERTURA");
		lblCobertura.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCobertura.setBounds(361, 24, 75, 14);
		contentPane.add(lblCobertura);
		
		JLabel lblArquibancada = new JLabel("ARQUIBANCADA");
		lblArquibancada.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblArquibancada.setBounds(10, 68, 90, 14);
		contentPane.add(lblArquibancada);
		
		JLabel lblBancoDeJogadores = new JLabel("BANCO DE JOGADOR");
		lblBancoDeJogadores.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBancoDeJogadores.setBounds(361, 68, 122, 14);
		contentPane.add(lblBancoDeJogadores);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 149, 694, 201);
		contentPane.add(scrollPane);
		
		tbQuadras = new JTable();
		scrollPane.setViewportView(tbQuadras);
		tbQuadras.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"N\u00FAmero", "Tipo", "Arquibancada", "Cobertura", "Banco de Jogador"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbQuadras.getColumnModel().getColumn(0).setResizable(false);
		tbQuadras.getColumnModel().getColumn(0).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(1).setResizable(false);
		tbQuadras.getColumnModel().getColumn(1).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(2).setResizable(false);
		tbQuadras.getColumnModel().getColumn(2).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(3).setResizable(false);
		tbQuadras.getColumnModel().getColumn(3).setPreferredWidth(90);
		tbQuadras.getColumnModel().getColumn(4).setResizable(false);
		tbQuadras.getColumnModel().getColumn(4).setPreferredWidth(120);
		
		// as linhas abaixo carregam a tabela de cadastro
		modelo = (DefaultTableModel) tbQuadras.getModel();
		modelo.setNumRows(0);
		
		String sql = "select * from tbquadras";
		try {
			pst = conexao.prepareStatement(sql);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				modelo.addRow(new Object[] {
						
						rs.getString(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
				});
			}
			
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
		JButton btnCadastrarQuadra = new JButton("Cadastrar");
		btnCadastrarQuadra.setBounds(606, 97, 98, 40);
		btnCadastrarQuadra.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				adicionar();
				// as linhas abaixo atualizam a tabela de cadastro
				modelo = (DefaultTableModel) tbQuadras.getModel();
				modelo.setNumRows(0);
				
				String sql = "select * from tbquadras";
				try {
					pst = conexao.prepareStatement(sql);
					rs = pst.executeQuery();
					
					while (rs.next()) {
						modelo.addRow(new Object[] {
								
								rs.getString(1),
								rs.getString(2),
								rs.getString(3),
								rs.getString(4),
								rs.getString(5),
						});
					}
							
				} catch (Exception a) {
					JOptionPane.showMessageDialog(null, a);
				}
			}
		});	
		contentPane.add(btnCadastrarQuadra);
		
	}
}

